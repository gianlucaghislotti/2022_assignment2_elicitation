# 2022_assignment2_Elicitation

## Membri del gruppo
Ghislotti Gianluca 859242<br>
Fortezza Carmine 861036<br>
Esposito Andrea 859732<br>

## Introduzione
Il prodotto software scelto per questo progetto è un'applicazione mobile, in particolare un'estensione dei sistemi già esistenti utilizzati dagli studenti universitari per il corretto svolgimento della loro carriera durante gli anni.

Riferendoci all'università Bicocca di Milano, attualmente per poter prenotare appelli, modificare il piano di studi, consultare l'orario, pagare le tasse, gli studenti devono utilizzare mezzi differenti per ciascuno scopo. Alcuni di questi possono essere il sito web, le segreterie online o pagine dedicate per orari e appelli, ma non esiste una piattaforma unica e rapidamente accessibile dove visualizzare tutte queste informazioni in modo semplice e veloce. Inoltre dato l'elevato numero di scadenze riguardanti pagamenti e prenotazioni da ricordare, molte di queste si possono dimenticare facilmente.

## Descrizione applicativo
La nostra applicazione vuole risolvere tutti questi problemi, creando un unico luogo per uno studente dove consultare le informazioni il più velocemente possibile e ricevere notifiche automatizzate nel caso di scadenze imminenti.

Questa app non vuole sostituire il sito web universitario, ma gestire esclusivamente le informazioni più importanti e di quotidiano utilizzo da parte degli studenti in modo da semplificarne la fruizione.

##### Funzionalità Principali
- Visualizzazione di appelli, esami, orari, tasse e dati personali
- Prenotazione degli appelli
- Conferma del voto in bacheca
- Pagamento delle tasse
- Modifica del piano di studi
- Selezione delle date per gli esami di lingua
- Statistiche sulla carriera universitaria
- Notifiche riguardanti scadenze imminenti  

## Competitors
Attualmente esiste un solo competitor di questo progetto, l'app *Uniwhere*. Questa applicazione mobile offre la possibilità di connettersi tramite email universitaria per accedere ai dati privati dello studente come: esami svolti, appelli e tasse da pagare. Il problema principale risiede nella poca cura all'esperienza utente che non risulta affatto semplificata, molte informazioni, infatti, sono disposte in maniera disordinata e i dati essenziali non vengono forniti in modo chiaro e rapido.

In particolare, non sono presenti notifiche automatizzate e non esiste un'interazione reale con l'università. L'applicazione consente semplicemente la visualizzazione dei dati.

## Stakeholders (A)
L'individuazione degli stakeholder è stata effettuata rispondendo alle domande di "Stakeholder Identification", per capire i potenziali problemi da risolvere oltre a chi è influenzato positivamente o negativamente dal progetto. Non tutti gli stakeholders scelti saranno effettivamente reperibili per cui definiremo un sottoinsieme per le tecniche di elicitazione.

- **Studenti**: Sono influenzati positivamente e direttamente essendo gli utenti finali. L'accesso alle infomazioni e lo svolgimento di operazioni quotidiane sarà semplificato e velocizzato notevolmente. 

- **Designer**: Saranno responsabili di curare la parte grafica e l'usabilità della nuova app. 

- **CINECA**: È un consorzio interuniversitario italiano responsabile dei dati degli studenti, dovrà essere contattato per poterli utilizzare nell'app.  

- **Università**: Viene influenzata positivamente avendo un'unica piattaforma da gestire e dove concentrare tutte le attenzioni.

- **Sviluppatori**: Per creare l'app avremo senz'altro bisogno di sviluppatori front-end e back-end, per dare importanza alla funzionalità così come all'esperienza utente.

- **Consulenti della privacy**: Saranno in grado di risolvere ogni problema legale riguardante il trattamento dei dati sensibili forniti dal CINECA.

- **Uniwhere**: È un competitor dell'app e quindi verrà influenzato negativamente da questo progetto, venendo eliminato il monopolio che detiene.

- **Project Manager**: Responsabile dell'assegnazione o dell'approvvigionamento di risorse o strutture. Avrà il pieno controllo decisionale sull'assegnamento delle posizioni lavorative.


| Power  | Intrest | Strategy | Stakeholders |
| ------------- | ------------- | ------------- | ------------- |
| High  | High  | Fully engage | Università, Project Manager |
| High  | Low  | Keep satisfied | CINECA, Consulenti della privacy, Sviluppatori, Designer |
| Low  | High  | Keep satisfied | Studenti |
| Low  | Low  | Minimum effort | |

## Workflow (B)

Per il nostro progetto abbiamo scelto di optare per tecniche di elicitazione **Artefact-Driven** come: il questionario, gli scenari, i prototipi e le storyboard; **Stakeholder-Driven** come le group session. Pensiamo che queste tecniche rappresentino meglio il system-as-is e il system-to-be mostrando dei documenti fisici agli stakeholders per ricevere dei feedback mirati sul nostro lavoro.

Nel diagramma sono presenti due tipologie di figure:
* rombo = if statement, la cui risposta influenza il flusso di lavoro del team.
* rettangolo = attività di elicitazione

<img src="/Workflow.png" alt="Workflow" width="40%"/>

Descrivendo un ipotetico flusso del diagramma, inizialmente si raccolgono dati utilizzando il **questionario informativo**, dal suo risultato dipenderanno le prime group sessions. Infatti il *project manager* avrà il compito di visualizzarlo e di avere l'ultima parola sulla sua bontà e utilizzo.<br>
Nel caso i dati non siano soddisfacenti, il questionario viene revisionato e modificato, infine inviato nuovamente agli studenti, altrimenti si procederà alle prime **group sessions**.<br>
Nella prima **unstructured group session** parteciperanno tutti i membri del team, i quali visualizzeranno i dati ricavati dal questionario informativo e inizieranno a creare una bozza dell'applicazione.<br>
La seconda **structured group session** sarà un allineamento con: il team, lo stakeholder *Cineca* e/o i **Consulenti della privacy**, i quali ci daranno una linea guida sul trattamento dei dati, così da rispettare le norme amministrative.<br>
Dopo aver concluso le group session si passerà alla creazione degli **senarios**.<br>
Lo step successivo sarà la creazione del **prototype & mockup**, una volta realizzati si avvierà uno svolgimento concorrente delle attività: <br>
- **Questionario mockup** il quale raccoglierà le preferenze degli utenti sull'artefatto creato.
- **Storyboards** i quali rappresentano l'interazione dell'utente con l'applicazione.<br>

Infine, viene effettuata una nuova **unstructured group session**, per mostrare a gli stakeholder interessati quanto creato finora. Qualora questa attività avesse esito positivo il workflow termina, in caso contrario verranno rieffettuate tutte le attività di elicitazione a partire dalla prima group session interna al team.<br>


### Questionario
1. Da questa attività di elicitazione ci aspettiamo di scoprire gran parte dei **requisiti funzionali**, quindi stabilire:
* Come il sistema dovrà comportarsi nelle funzionalità più utilizzate.
* Quali specifiche funzionalità dovrà contenere.
* Quali funzionalità sono poco apprezzate nelle piattaforme attuali, così da escludere e modellarle nella maniera più opportuna possibile.

2. Gli stakeholder coinvolti in questa attività sono gli **Studenti**, i quali hanno maggiore interesse rispetto agli altri membri, dato che utilizzerebbero il prodotto quotidianamente. Quindi hanno sicuramente idee chiare sull’applicativo finale sulla base delle loro esperienze personali con le piattforme attualmente in vigore. <br>
Abbiamo inviato agli studenti il questionario tramite la piattaforma Whatsapp.<br>
Le *domande* sono state scritte in modo da far convergere lo studente ad una risposta netta, quindi ad una presa di posizione ben precisa ed evitare la neutralità.<br>
Le loro *risposte* quindi, sono di grande importanza nel farci capire i **punti cardine** dello sviluppo.Inoltre dai risultati ottenuti saremo in grado di effettuare delle scelte funzionali ben precise e mirate alle loro preferenze, dandone la giusta importanza e rilevanza in base al numero di risposte positive. 

3. Abbiamo deciso di posizionare i questionari come **prima attività** perché grazie alle informazioni che otterremo, saremo in grado di comporre efficientemente le attività successive. In particolare gli *scenarios* e i *prototipi*; i quali senza queste informazioni, avrebbero il rischio di intraprendere strade non coerenti e corrette con il volere degli utenti finali.

### Group sessions
Con questa attività cercheremo di scoprire con stakeholder diversi,  nuovi requisiti quali ad esempio: nuove funzionalità dell’applicazione, ulteriori statistiche che potrebbero interessare agli studenti, quali livelli di sicurezza implementare, come strutturare tutte le funzionalità dell’applicazione…

Inoltre, in questa attività di elicitazione potrebbe emergere che alcuni requisiti precedentemente individuati non siano importanti e quindi non da studiare e implementare.

Abbiamo individuato due group session differenti con **Stakeholder** differenti:

- **Unstructured group session con Sviluppatori, Project Manager e Designer**.<br>
Questa group session ha lo scopo di generare idee su come l’applicazione deve essere strutturata, cosa deve contenere… <br>
Gli **Sviluppatori** e i **Designer** partecipano attivamente in quanto dovranno poi sviluppare e disegnare quanto discusso. Possono essere utili nello scoprire nuovi requisiti e aiutare con la fattibilità delle operazioni.<br>
Il **Project Manager** ha un anch’esso un ruolo attivo, dando il suo contributo nello scoprire nuovi requisiti ed eliminare quelli superflui.<br>
- **Structured group session con Sviluppatori, Project Manager e Cineca e/o Consulenti della privacy**: <br>
Questa group session ha lo scopo di discutere insieme al **Cineca** ed eventualmente ai **Consulenti della privacy** come si è pensato di trattare i dati degli studenti
- **Group session** = l'ultima attività del workflow.
In questo caso gli **stakeholder** hanno dei **ruoli ben definiti**.<br>
Si è deciso di effettuare questa group session in quanto da qui potrebbe emergere la necessità di implementare **ulteriori livelli di sicurezza**.

### Scenarios
In questa attività si formalizza tramite diagrammi o in forma testuale quanto individuato e discusso durante la precedente group session. Potrebbero emergere dei nuovi requisiti dato che, durante la group session potrebbe essere sfuggito qualche passaggio su come debba essere sviluppata la una nuova attività. È importante tenere sempre a mente che i diagrammi devono essere semplici e quindi non troppo carichi di informazioni. <br>
Gli stakeholder coinvolti in questa attività sono gli Sviluppatori (1 o 2 al massimo), in quanto i diagrammi devono essere semplici e non troppo carichi di informazioni.

Questa attività si posiziona **dopo una group session** e **prima** della fase di **prototype e mockup**. Questo perché gli scenari e anche la precedente group session possono essere d’aiuto per realizzare la fase successiva; Infatti, anche i **designer** partecipano alla group session precedente con gli **Sviluppatori** e il **Project Manager**.<br>

Per questa attività è molto importante rappresentare quanto sopra descritto con dei **diagrammi** per facilitare la scoperta di nuovi requisiti. Inoltre, bisogna rappresentare solamente le interazioni e i diagrammi che riteniamo utili per scoprire nuovi requisiti, altrimenti si rischia di incorrere in una non necessaria sovra specificazione.


### Prototype
In questa fase sono stati scelti gli *High-Fidelity Prototypes*, ovvero degli artefatti che assomigliano molto al prodotto reale e che saranno presenti nelle fasi finali del workflow. 

I requistiti che ci siamo prefissati di raccogliere da questa attività consentono una valutazione da parte degli utenti dell'intera interfaccia grafica, quindi come le informazioni saranno mostrare sullo schermo oltre all'interazione con le componenti presentate. L'interattività dei prototipi è fondamentale perchè gli utenti possono testare i gesti e le funzioni tipiche dei dispositivi mobili prima che il prodotto sia effetivamente sviluppato.

La prototipazione coinvolge tutti i membri del team, questo assicura che il prototipo finale soddisfi gli obiettivi di prodotto e non sia in contraddizione con le esigenze degli utenti o con i requisiti tecnici. Vengono esclusi gli stakeholders che sono interessati esclusivamente alla gestione dei dati e non all'app in se ed in particolare:
- Gli Sviluppatori saranno interessati all'attività perchè dovranno successivamente sviluppare ciò che sarà presentato e gestire eventuali animazioni ed interazioni.
- I designer saranno responsabili della creazione dei prototipi avendo le compentenze di UI/UX necessarie.
- L'Università ha un contributo importante sul design finale e sul branding dato che sarà un'app esclusivamente per Bicocca.
- Gli Studenti avranno interesse nel vedere i prototipi perchè saranno i primi ad utilizzare l'app e sarà necessario studiare accuratamente le loro risposte in termini di usabilità ed interazione. Saranno loro a generare i feedback più importanti sull'effettivo valore della nuova app rispetto alla precedente.
- Il Project Manager dovrà capire se i prototipi rispetteranno tutti i requisiti e tutti i vincoli di progetto preposti.

I principali motivi per il quale questa fase è stata scelta sono:
- Consentono una valutazione più approfondita delle componenti, della navigazione e del coinvolgimento dell'utente. 
- Gli utenti possono fornire feedback rapidi e diretti sull'interfaccia.
- Anche gli stakeholders hanno un'idea chiara di come sarà l'app dopo il lancio.

Di solito questa attività entra in scena nelle fasi finali di elicitazione perchè riunisce tutti i requisiti raccolti e gli scenari possibili. I prototipi rappresentano come queste informazioni saranno mostrate effettivamente e come gli utenti interagiranno con esse. Questa fase include anche i feedback e serve per capire come i bisogni generali siano stati percepiti nel produrre un prodotto finale.

### Storyboard
Lo Storyboard forniscsce l'intero quadro dell'ambiente in cui gli utenti interagiscono con il prodotto. Vogliamo presentare frammenti specifici di un percorso utente attraverso immagini o sketch per guidarli nella comprensione del funzionamento dell'applicazione.

Per noi è fondamentale prestare attenzione alla funzionalità, all'usabilità e all'interfaccia.<br>
Queste fasi di elicitazione permetteranno di creare un prodotto estremamente facile da usare, punto debole invece per le piattaforme universitarie attualmente disponibili.

Questa attività mette gli stakeholders nei panni degli utenti per poter capire come gli studenti riusciranno a svolgere le attività quotidiane nella maniera più semplice e veloce possibile. Abbiamo scelto come stakeholders gli stessi che saranno presenti nella la fase di prototype.

Anche questa fase è posta alla fine del workflow perchè tratta l'usabilità del prodotto. Le varie storyboard dovranno essere quindi mostrate solo dopo tutte le altre tecniche di elicitazione.

## Presentazione del questionario (C)

Il questionario è stato suddiviso in **quattro sezioni**:
1. Domande generali
2. Unimib Course
3. Segreterie Online
4. Nuova Applicazione Unimib

Ognuna delle quali è adibita a reperire informazioni ben precise. <br>
Alla fine della sezione **introduzione** viene posta una domanda che chiede all'utente se utilizza l'applicazione UnimibCourse, in base alla risposta che si riceve, visualizzerà delle domande piuttosto che altre, in particolare se risponde **si** esso passa alla sezione **due**, altimenti passa alla **tre**.<br> 
A questo punto le domande nelle sezioni successive vengono visualizzate in modo sequenziale.

Di seguito viene descritto il motivo per il quale sono state impostate quel **tipo di risposte** :
* **Checkbox** = da la possibilità a chi compila di selezionare risposte multiple, così facendo è possibile stabilire la distribuzione delle opzioni da noi proposte.
* **Rating** = ci consentirà di stabilire l'importanza della risposta per l'utente, su una scala da 1 a 5.
* **Radio button** = usato nelle risposte alle domande in cui lo studente deve prendere una posizione netta e definita.

### Domande generali
Le domande presenti in questa sezione sono di carattere generale, per indirizzare l'utente verso domande più specifiche, nelle quali estrapolaremo anche informazioni relative alla presenza di più piattaforme universitarie.

In questo caso l'obiettivo consiste nella raccolta di dati generali degli studenti, in modo da avere un riferimento sui dispositivi maggiormente utilizzati. Abbiamo deciso di porre questa domanda per verificare se un’applicazione mobile verrebbe utilizzata spesso.<br>
<img src="/Immagini/Introduzione/1.JPG" alt="Introduzione_Img_1" width="600"/> 

Questa domanda è utile per capire se agli studenti possa interessare un’unica piattaforma dove vengono raggruppati tutti i servizi universitari così da poter facilitare la visualizzazione e la raccolta delle informazioni.<br>
<img src="/Immagini/Introduzione/2.JPG" alt="Introduzione_Img_2" width="600"/> 

Questa domanda viene posta per capire se lo stakeholder in questione utilizza o meno l’applicazione UniMib Course. In base alla risposta che si riceve, quest’ultimo visualizzerà o meno ulteriori domande.<br>
<img src="/Immagini/Introduzione/3.JPG" alt="Introduzione_Img_3" width="600"/> 

### UniMiB Course
La seguente sezione tratta domande di natura informativa sull’applicazione universitaria attualmente in vigore, “UniMiB Course”. Si vogliono evidenziare i punti deboli ed esaltare i punti di forza, così da indicarci la via corretta sull'approccio dello sviluppo dell’interfaccia. I requisiti che scopriremo in questa sezione sono sia funzionali che non funzionali.

Oltre a stabilire se l'applicazione esistente viene utilizzata, vogliamo capire quanto questo sia importante. Con questa domanda siamo in grado di capire se l'app sia di quotidiano utilizzo oppure no.<br>
<img src="/Immagini/UnimibCourse/1.JPG" alt="UnimibCourse_Img_1" width="600"/><br>

In questa domanda è stato chiesto quale delle tre sezioni dell’app venga usata, così da estrapolarne l’importanza. Capiremo quale delle sezioni ha più importanza e quindi il motivo per il quale l'app è usata abitualmente piuttosto che raramente.<br>
<img src="/Immagini/UnimibCourse/2.JPG" alt="UnimibCourse_Img_2" width="600"/><br>

Queste tre domande permettono di capire se quella determinata pagina sia intuitiva per l'utente. Nello stesso tempo è presente un'opzione *Non lo so*, se la risposta viene selezionata vuole indicare che l'utente non ha mai visto o utilizzato la sezione dell'applicazione proposta, quindi quella pagina non è stata capita.<br>
<img src="/Immagini/UnimibCourse/3.JPG" alt="UnimibCourse_Img_3" width="600"/><br>
<img src="/Immagini/UnimibCourse/4.JPG" alt="UnimibCourse_Img_4" width="600"/><br>
<img src="/Immagini/UnimibCourse/5.JPG" alt="UnimibCourse_Img_5" width="600"/><br>

Questa affermazione permette di capire, basandoci sulle risposte dell’utente, se per quest'ultimo sarebbe più significativo entrare con la mail universitaria, oppure se inserire i dati manualmente non risulta poco agevole.<br>
<img src="/Immagini/UnimibCourse/6.JPG" alt="UnimibCourse_Img_6" width="600"/><br>

Queste affermazioni vogliono estrapolare informazioni di natura generale sull’applicazione universitaria attuale, per capire se all'utente non manca nulla da aggiungere e se è in generale intuitiva da utilizzare.<br>
<img src="/Immagini/UnimibCourse/7.JPG" alt="UnimibCourse_Img_7" width="600"/><br>
<img src="/Immagini/UnimibCourse/8.JPG" alt="UnimibCourse_Img_8" width="600"/><br>

### Segreterie Online
La seguente sezione tratta domande relative alle segreterie online, in particolare la reazione da parte degli stakeholder all’intuitività del sito ed alle funzioni presenti. Lo scopo è quello di raccogliere informazioni riguardo le loro esperienze con questa piattaforma, in modo tale da preparare una strategia di sviluppo in grado di soddisfare le richieste degli studenti. Vogliamo essere sicuri di dare la corretta importanza ad ogni funzionalità presente, basandoci sulla continuità del loro utilizzo e scoprendo i requisiti funzionali più importanti che comporranno la nuova applicazione universitaria.

Considerando che alcuni task che uno studente svolge, sono eseguibili solo attraverso questa piattaforma, abbiamo chiesto agli studenti se si trovano a loro agio con l'utilizzo delle segreterie online.<br>
<img src="/Immagini/SegreterieOnline/1.JPG" alt="SegreterieOnline_Img_1" width="600"/><br>

In questo caso esprimiamo due problematiche importanti presenti nel sito: la modifica del piano di studi e l'eccessiva numerosità di date da ricordare.<br>
<img src="/Immagini/SegreterieOnline/2.JPG" alt="SegreterieOnline_Img_2" width="600"/>
<img src="/Immagini/SegreterieOnline/3.JPG" alt="SegreterieOnline_Img_3" width="600"/><br>

### New UniMiB
La seguente sezione mira ad esplorare i pareri studenteschi, circa nuove funzionalità che vorremmo inserire. In particolare la tematica *notifiche* e quella riguardante le funzionalità più importanti che saranno presenti.

Questa domanda è molto efficace per scoprire nuove funzionalità da implementare nell’applicazione. Inoltre, potrebbe emergere che alcune funzionalità pensate dal team di sviluppo non risultino interessanti per gli utenti finali.<br>
<img src="/Immagini/NewUnimib/1.JPG" alt="NewUnimiBib_Img_1" width="600"/>

Questa domanda viene posta per capire se agli studenti piacerebbe ricevere delle notifiche e se sì, su quali sezioni. Ci aiuterà a capire quali delle possibili notifiche potrebbero risultare più utili per gli studenti.<br> 
<img src="/Immagini/NewUnimib/2.JPG" alt="NewUnimiBib_Img_2" width="600"/>

## Presentazione dei Prototipi
In questa fase vengono creati i prototipi secondo le risposte date dagli studenti nel questionario. Di seguito verranno presentate tutte le interfacce pagina per pagina.

### Oggi
*Oggi* è la pagina principale dell'applicazione, ed è stata creata come riepilogo dei dati da visualizzare rapidamente oltre ad evidenziare le informazioni più importanti della giornata. Inoltre è presente una sezione per visualizzare le statistiche dello studente come la media e base di laurea.

<img src="/Immagini/Prototipi/oggi1.png" alt="oggi1" width="330"/><img src="/Immagini/Prototipi/oggi2.png" alt="oggi2" width="330"/>

### Esami
Questa pagina contiene tutti gli esami dello studente suddivisi in *passati*, *rimanenti* e *in attesa*. Sarà quindi possibile visualizzare il libretto universitario e conoscere quali degli esami devono ancora essere superati. Un'altra sezione aggiunta è la sezione *In attesa*, che mostra gli esami in correzione e quindi già dati, ma non ancora passati o quelli ancora da verbalizzare.

<img src="/Immagini/Prototipi/esami1.png" alt="esami1" width="330"/><img src="/Immagini/Prototipi/esami2.png" alt="esami2" width="330"/>

### Orario
L'orario è una delle parti più importanti di quest'applicazione. Le informazioni sono state semplicemente ridisegnate perchè la precedente versione aveva già una buona base. Quando una lezione viene annullata è subito visibile e sono presenti le indicazioni delle pause per capire la distanza tra le lezioni. Infine è stato aggiunto un bottone per trovare le indicazioni dell'aula in cui si terrà la lezione. 

<img src="/Immagini/Prototipi/orario.png" alt="orario" width="330"/><br>

### Appelli
La pagina *Appelli* permette una suddivisione chiara tra gli appelli *prenotati*, quelli *disponibili per la prenotazione* e i *futuri*, ovvero quelli a cui non ci si può ancora iscrivere. Come nelle segreterie online è possibile prenotare o togliere la prenotazione, ma in modo decisamente più chiaro.

<img src="/Immagini/Prototipi/appelli.png" alt="appelli" width="330"/><br>

### Tasse
La sezione *Tasse* permette la visualizzazione dei pagamenti effettuati e non, inoltre da la possiblità di pagare dallo smartphone direttamente dall'applicazione. Questa pagina è utile per semplificare il processo di pagamento e per notare immediatamente quali di queste hanno un immediata scadenza.

<img src="/Immagini/Prototipi/tasse.png" alt="tasse" width="330"/><br>

### Profilo
Il *Profilo* è utilizzato per informazioni di minore importanza e non visualizzabili quotidianamente, come il piano di studi e i dati personali dello studente. Inoltre sono presenti il progetto universitario individualizzato e la dichiarazione ISEE.

<img src="/Immagini/Prototipi/profilo.png" alt="profilo" width="330"/><br>

## Analisi delle risposte (D)
I risultati ottenuti sono suddivisi nelle 4 sezioni descritte al punto C, seguirà una descrizione dei risultati.

### Domande generali
Dai risultati ottenuti da questa domanda si nota che gli studenti utilizzano maggiormente il **computer** e il **telefono** per accedere ai servizi universitari. Quindi, per sviluppare la nostra idea si potrebbe costruire un **sito web** oppure **un'applicazione** piuttosto che una **progressive web app**, la quale può essere **installata/aggiunta** direttamente sui cellulari e raggiunta tramite **web browser** dai computer.
Lo smartphone in seconda posizione, è spiegabile dal fatto che la maggioranza delle funzionalità è situata esclusivamente sul sito web, non particolarmente adatto per i piccoli schermi.

<img src="/Dati/Introduzione/2.JPG" alt="Introduzione_Img_2" width="600"/><br>

Analizzando questi risultati, abbiamo rilevato che circa il **72%** degli studenti è d'accordo con la nostra affermazione, ovvero che la disgregazione dei servizi risulta dispersiva. Il restante **28%** apprezza questa suddivisione.
Di conseguenza, questi dati ci aiutano a capire che gli studenti **apprezzerebbero molto** un'applicazione come la nostra, la quale punta a convergere le funzionalità di tutte le piattaforme in un unico applicativo accessibile da smartphone.

<img src="/Dati/Introduzione/3.JPG" alt="Introduzione_Img_3" width="600"/><br>

### UniMiB Course 
Da questi dati emerge che l'**86,4%** degli studenti utilizza abitualmente l'applicazione **Unimib Course**, di conseguenza ci aspettiamo delle risposte non generiche alle prossime domande.

<img src="/Dati/UnimibCourse/1.JPG" alt="UnimibCourse_Img_1" width="600"/><br>

Dai dati ottenuti da questa domanda emerge che la maggior parte degli studenti utilizza l'applicazione per consultare gli orari delle lezioni (circa il **95,5%**), di conseguenza questa è una feature importante da implementare, mentre le aule disponibili sono controllate dal **50%** degli studenti. 

<img src="/Dati/UnimibCourse/2.JPG" alt="UnimibCourse_Img_2" width="600"/><br>

Analizzando i dati ottenuti da questa domanda possiamo notare che che c'è una **forte correlazione** tra gli studenti che utilizzano l'applicazione per consultare le aule (50%) e la percentuale di studenti che ritengono la pagina "aule" di facile consultazione(50%). Quello che possiamo dedurre è che quasi tutti gli studenti che consultano la sezione aule ritengono che la visualizzazione sia **semplice**.<br>
Considerato ciò, potrebbe aver senso implementare la sezione "aule" nella nostra applicazione.<br>
Abbiamo inoltre una fetta importante di studenti (circa il 22,7%) che **non ha mai utilizzato la sezione**.

<img src="/Dati/UnimibCourse/3.JPG" alt="UnimibCourse_Img_3" width="600"/><br>

Da questi dati emerge che:
- il 45,5% degli studenti non ha mai utilizzato la sezione o non ne era a conoscenza.
- il 18,2% degli studenti non ritiene di facile consultazione la sezione esami dell'applicazione.
- il 36,4% degli studenti ritiene di facile consultazione la sezione.<br>
Notiamo una **discrepanza** nei dati, in quanto nella prima domanda della sezione (UniMib Course) abbiamo ottenuto che una percentuale molto bassa di studenti utilizzano questa pagina, mentre qui otteniamo che il 36,6% la ritiene di facile consultazione.
Riteniamo quindi che questo tema sia da **approfondire**, per capire se gli studenti non si trovino bene con l'usabilità della pagina oppure che non abbia una reale utilità.<br>

<img src="/Dati/UnimibCourse/4.JPG" alt="UnimibCourse_Img_4" width="600"/><br>

Dai risultati di questa domanda otteniamo che la maggior parte degli studenti (86,4%) ritiene la sezione "orario" dell'applicazione di facile consultazione e considerando che nella risposta ottenuta precedentemente il 95,5% degli studenti utilizza la sezione orario deduciamo che dovremmo **sicuramente** implementare questa sezione all'interno della nostra applicazione.

<img src="/Dati/UnimibCourse/5.JPG" alt="UnimibCourse_Img_5" width="600"/><br>

Studiando i dati ottenuti da questa domanda deduciamo che i **pareri degli studenti sono in forte contrasto**:
- Circa il 54.5% degli studenti è in disaccordo con quanto da noi affermato.
- La restante percentuale è invece d'accordo.<br>
Riteniamo però che la **soluzione migliore** per recuperare i dati della carriera degli studenti sia farlo in modo **automatico**, così che lo studente non debba inserirli manualmente ogni anno oppure ad ogni modifica del piano di studi. Bisognerebbe capire quale delle due alternative preferiscono utilizzare per accedere all'app con una domanda specifica.

<img src="/Dati/UnimibCourse/6.JPG" alt="UnimibCourse_Img_6" width="600"/><br>

Anche i dati ottenuti per questa domanda risultano in **forte contrasto**:
- 45,5% ritene che l'applicazione sia ben riuscita e completa.
- 54,5% ritiene che l'applicazione **NON** lo sia.<br>
Riteniamo che le funzionalità integrative da noi pensate e individuate renderebbero il parere degli studenti più **omogeneo**.

<img src="/Dati/UnimibCourse/7.JPG" alt="UnimibCourse_Img_7" width="600"/><br>

Analizzando questa domanda, otteniamo che la maggioranza degli studenti ritiene che l'applicazione già esistente sia **intuitiva**, di conseguenza per sviluppare la nostra applicazione dovremmo prendere spunto da quest'ultima, creando un'applicazione **semplice**, **user friendly** e **intuitiva**.

<img src="/Dati/UnimibCourse/8.JPG" alt="UnimibCourse_Img_8" width="600"/><br>

### Segreterie Online
Dal **56%** riportato si può evincere che c’è una divergenza di pensiero tra gli studenti circa l’intuitività del sito delle segreterie online. Anche qui bisogna approfondire la domanda per capire le ragioni per le quali alcuni pensano sia un sito intuitivo mentre altri no.

<img src="/Dati/Segreterie/1.JPG" alt="Segreterie_Img_1" width="600"/>

Per il **68%** degli studenti risulta semplice ricordare tutte le scadenze. Sembra abbastanza ovvio che la loro risposta si avvale sulla facoltà di memoria, mentre invece, non è previsto alcun supporto per ricordare queste date. Si può notare che per il **32%** degli studenti non è semplice ricordare le date. Una domanda specifica potrebbe essere posta per capire la natura della risposta.

<img src="/Dati/Segreterie/2.JPG" alt="Segreterie_Img_2" width="600"/>

Il **72%** degli studenti è in disaccordo con questa affermazione, sintomo di una urgente necessità di ridisegnare il piano di studi, rendendo la sua modifica più semplice e comoda. Sarà quindi una funzionalità importante per la nuova applicazione.

<img src="/Dati/Segreterie/3.JPG" alt="Segreterie_Img_3" width="600"/>

### New UniMiB
Dai dati ricavati è possibile stabilire quali funzionalità sono più richieste di altre. La prima è sicuramente il libretto scolastico, scelta dall'**80%** degli studenti; sopra il **60%** abbiamo la prenotazione e visualizzazione degli appelli, la bacheca esiti ed infine la possibilità di effetuare pagamenti direttamente dall'app. Più della metà degli studenti vorrebbe tutte le funzionalità del sito, mentre la gestione dei questionari non sembra essere interessante: circa 2 ragazzi su 10 l'hanno scelta.

<img src="/Dati/NewUnimib/1.JPG" alt="NewUnimib_Img_1" width="600"/>

Da queste risposte possiamo notare un dato importante, ovvero, che tutti gli studenti vorrebbero almeno una notifica automatica. La notifica più richiesta è quella relativa ad una variazione dell'orario scolastico scelta dall'**88%** delle persone. Quasi tutte le notifiche sono richieste da più di 6 ragazzi su 10, quelle meno utili sono l'avviso di un esame in arrivo e l'iscrizione ad esami bloccanti (**44%**-**52%**). Tutte le notifiche sono state scelte da quasi il **50%** dei ragazzi e questo indica che la metà di tutti gli studenti hanno bisogno di una notifica.

<img src="/Dati/NewUnimib/2.JPG" alt="NewUnimib_Img_2" width="600"/>

## Conclusioni
Da studenti ci siamo posti alcune domande su come migliorare l'esperienza universitaria di uno studente, semplificando i mezzi esistenti e riunendoli in un unica piattaforma rapidamente accessibile. Dopo aver riassunto i problemi principali, abbiamo deciso di esporli attraverso un questionario, per capire le opinioni degli altri studenti. Successivamente i dati sono stati interpretati, ma ci siamo accorti che per alcune domande sarebbe meglio approfondire il motivo delle risposte date in modo da avere dati inequivocabilmente interpretabili. Con approfondire intendiamo rendere le domande più specifiche sulla base delle risposte che abbiamo ricevuto.
